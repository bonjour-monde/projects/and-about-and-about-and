**AND ABOUT AND ABOUT AND**<br>
:: LISAA Graphisme - G4_CT<br>
:: 4 → 8 décembre 2023<br>

————————————<br>
Grâce à l’évolution des logiciels, standards, et technologies web, de plus en plus d’identités graphiques se construisent autours de systèmes typographiques atypiques, voir variables. La lettre devient donc un objet vivant et évolutif, capable de surprendre même son designer. 	<br>							
En prétextant la création d’une identité d’un festival de musique procédurale, nous explorerons la conception d’un caractère typographique génératif à 50 mains. Chaque étudiant devra traduire une source d’inspiration sonore en rythmes graphiques puis en une police de caractère complète. Chaque lettre sera pensée à la fois comme élément de lecture et élément graphique abstrait. Nos scripts permettront ensuite le mix automatisé de plusieurs caractères, le tout dans un système combinatoire en perpétuelle variation.<br>
 			<br>	   
————————————<br>			
**Livrables**	<br>		
— Différents caractères typographiques, chacun divisé en deux fichiers WOFF<br>
— Des visuels promotionnels faisant le lien entre les inspirations sonores de départ et les productions typographiques.	<br>
<br>
————————————<br>
**Agenda**	<br>		
**J1**<br>
Matin : Présentation du travail de Bonjour Monde et de la notion de lettre variable<br>
Après-midi : Exploration sonore et graphique à la main<br>
**J2**<br>
Matin : Fin exploration graphique à la main<br>
Après-midi : Introduction au logiciel Glyphs et aux méthodes de constructions combinatoires.<br>
**J3**<br>
Matin : Travail sur glyphs<br>
Après-midi : Travail sur Glyphs et première mise en commun des caractères combinatoires.<br>
**J4**<br>
Matin : Travail sur glyphs<br>
Après-midi : Travail sur Glyphs  et seconde mise en commun des caractères<br>
**J5**<br>
Matin : Finalisation sur Glyphs, export final et mise en commun des caractères.<br>
Après-midi : Conception des visuels promotionnels et restitution des résultats.<br>

————————————<br>
**I N T E R  F A C E**<br>
• Télécharger le [fichier ZIP](https://gitlab.com/bonjour-monde/projects/and-about-and-about-and/-/blob/main/sources/about-and-about-and-about.zip?ref_type=heads) dans le dossier `source`<br>
• Exporter les fichiers au format WOFF selon la nomenclature suivante :<br>
— Nom de famille : **Capela** (ça ne change pas)<br>
— Nom de style : **Style** (à vous de décider) **A** (pour le calque A) et **B** (pour l'autre, facile)<br>
• Placer le fichier A dans le dossier du même nom (`assets/fonts/A`), pareil pour B (`assets/fonts/B`)<br>
• Ajouter le nom à la liste dans le fichier Javascript (`assets/js/scripts.js`)<br>
—— En fin de chaque journée :<br>
• Ajouter ce même nom à la [liste commune](https://semestriel.framapad.org/p/lisaa-andabout)<br>
• Placer le fichier A [sur le Drive](https://drive.google.com/drive/folders/1ytc_TrgWZIvsaFH7RmbeqWIRGfHr5RVF) dans le dossier du même nom (`assets/fonts/A`), pareil pour B (`assets/fonts/B`)<br>
/!\ Attention aux doublons dans la liste de noms, pas besoin de remettre ce qui y est déjà.<br>
/!\ Bien re-uploader les fichiers typo sur le Drive s'ils ont été modifiés, qu'on finisse chaque journée avec la dernière version.<br>

————————————<br>
**L I E N S**

• [Télécharger le logiciel Glyphs](https://glyphsapp.com/buy)<br>
• [Ressources typographiques diverses (rassemblées par Studio Triple)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/type%20design%20education%20ressources.md)<br>
• [Tutoriels pour le logiciel Glyphs](https://glyphsapp.com/learn)<br>
• [Oh No Type Co. blog](https://ohnotype.co/blog/tagged/teaching)<br>
• [Vieux spécimens typographiques numérisés (rassemblés par Emmanuel Besse)](https://www.are.na/emmanuel-besse/type-specimens-taleuy1p7xc)<br>
• [Type Review Journal](https://fontreviewjournal.com/)<br>
• [Fonts In Use](https://fontsinuse.com/)<br>
• [Font of the Month Club](http://fontofthemonth.club/)<br>
• [Stratégies italiques](http://strategiesitaliques.fr/)<br>
• [Guide pour le spacing, Fontsmith](https://www.fontsmith.com/blog/2018/02/05/how-to-space-a-typeface)<br>
• [Guides pour le spacing, Society of Fonts](https://www.societyoffonts.com/2018/09/26/spacing-a-font-part-2/)<br>
• [Article sur le spacing, OHno](https://ohnotype.co/blog/spacing)<br>
<br>
————————————<br>
**R É F É R E N C E S**

MODULES<br>
• [MODULES > Capitales (Edward Catich, ‘The Origin of the Serif’)](https://pampatype.com/thumbs/blog/reforma/trazos-pincel-catich-1200x536.png)<br>
• [MODULES > Capitales (Corentin Noyer, ‘Manipulation Typographique’)](https://anrt-nancy.fr/anrt-22/media/pages/projets/manipulation-typographique/b5232b4de5-1650381072/anrt-2015-2016-noyer-03.jpg)<br>
• [MODULES > bas-de-casse (Studio Triple, ‘Modules et anatomie’)](https://gitlab.com/StudioTriple/vite_et_mal/-/blob/master/documentation/01%20modules%20et%20anatomie.pdf)<br><br>
FONDERIES / SPÉCIMENS WEB<br>
• [Spécimens typographiques web](https://typespecimens.xyz/specimens/all/)<br>
• [Commercial Classics Showcase](https://showcase.commercialclassics.com/)<br>
• [OHno Type Co.](https://ohnotype.co/fonts)<br>
• [General Type Studio](https://www.generaltypestudio.com/)<br>
• [The Pyte Foundry](https://thepytefoundry.net/)<br><br>



